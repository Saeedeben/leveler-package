<?php

namespace Eben\Level\Database\Factories;

use App\Models\Customer\Level;
use Illuminate\Database\Eloquent\Factories\Factory;

class LevelFactory extends Factory
{
	protected $model = Level::class;

	public function definition()
	{
		return [
			//
		];
	}
}