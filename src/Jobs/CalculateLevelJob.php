<?php

namespace Eben\Level\Jobs;

use App\Models\Customer\Level;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CalculateLevelJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * @var Level $level
	 */
	public $level;

	public function __construct($level)
	{
		$this->level = $level;
	}

	public function handle()
	{
		$points       = $this->level->points;
		$alreadyLevel = $this->level->level;
		$pointRate    = config('leveler.point_rate');

		$newLevel = (($alreadyLevel - 1) * $pointRate) + ($alreadyLevel * $pointRate);

		if ($points >= $newLevel) {
			$this->level->update(['level' => $alreadyLevel + 1]);
		}

	}
}