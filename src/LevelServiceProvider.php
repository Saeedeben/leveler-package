<?php

namespace Eben\Level;

use Illuminate\Support\ServiceProvider;

class LevelServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->app->bind('leveler', function ($app) {
			return new Leveler();
		});
	}

	public function boot()
	{
		$this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

		if (!class_exists('Level')) {
			$this->publishes([
				__DIR__ . '/Models/Level.php' => app_path('/Models/Customer/Level.php'),
			], 'models');
		}

		$config_path = __DIR__ . '/../config/leveler.php';
		$this->publishes([
			$config_path => config_path('leveler.php'),
		], 'config');
	}
}