<?php

namespace Eben\Level;

use App\Models\Customer\Level;
use Eben\Level\Jobs\CalculateLevelJob;

class Leveler
{
	public function updateLevel(Level $level)
	{
		return CalculateLevelJob::dispatch($level);
	}

	public function addPoints($customerId, $point)
	{
		$customerLevel = Level::query()
			->where('customer_id', $customerId)
			->first();

		$customerLevel->points += $point;
		$customerLevel->save();
	}

}