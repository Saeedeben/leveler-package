<?php

namespace App\Models\Customer;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 *
 * @property int      $id
 *
 * @property int      $level
 * @property int      $points
 * @property int      $player_id
 *
 * @property Carbon   $created_at
 * @property Carbon   $updated_at
 *
 * ------------------------------------ Relations ------------------------------------
 * @property Customer $customer
 */
class Level extends Model
{
	use HasFactory, SoftDeletes;

	protected static function newFactory()
	{
		return \Eben\Level\Database\Factories\LevelFactory::new();
	}

	protected $guarded = [];

	protected $fillable = [];

	// ------------------------------------ Relations ------------------------------------
	public function customer()
	{
		return $this->belongsTo(Customer::class);
	}
	// ------------------------------------ Attributes ------------------------------------
	// ------------------------------------ Methods ------------------------------------
	// ------------------------------------ Scopes ------------------------------------

}