<?php

namespace Eben\Level\Facades;

use Illuminate\Support\Facades\Facade;

class Leveler extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'leveler';
	}
}